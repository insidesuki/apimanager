<?php

namespace Insidesuki\ApiManager\Authentification\Contracts;

interface ApiClientInterface
{

	public function setApiName(string $apiName):void;

	public function getApiName():string;

	public function setBaseUrl(string $url):void;

	public function getBaseUrl():string;




}