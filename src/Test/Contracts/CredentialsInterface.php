<?php

namespace Insidesuki\ApiManager\Test\Contracts;
interface CredentialsInterface
{

	public function contentType(): string;
	public function baseUrl():string;
	public function authUrl():string;
	public function username():string;
	public function password():string;
	public function ttl(): int;


}