<?php
declare(strict_types = 1);

namespace Insidesuki\ApiManager;

use Insidesuki\ApiManager\Authentification\BearerAuthenticator;
use Insidesuki\ApiManager\Authentification\Contracts\ApiBearerClientInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class DefaultBearerRepository extends AbstractApiRepository
{


	public function __construct(
		ApiBearerClientInterface $bearerClient,
		HttpClientInterface $httpClient)
	{

		$authClient = new BearerAuthenticator(
			$httpClient, $bearerClient
		);
		parent::__construct($httpClient, $authClient);
	}


	public function find(string $url): ?array
	{
		return parent::find($url);

	}

	public function put(string $url, array $data): array
	{
		return parent::put($url, $data);
	}

	public function post(string $url, array $data)
	{
		return parent::post($url, $data);
	}

}