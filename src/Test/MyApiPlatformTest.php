<?php
declare(strict_types = 1);

namespace Insidesuki\ApiManager\Test;
use ApiPlatform\Symfony\Bundle\Test\ApiTestCase;
use ApiPlatform\Symfony\Bundle\Test\Client;
use JsonException;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

/**
 * TestCase for api calls
 */
abstract class MyApiPlatformTest extends ApiTestCase
{
	protected FilesystemAdapter $cacheStore;
	protected int               $ttl;
	protected string            $tokenCacheName = 'api.test';

	private array  $options = [];
	private string $urlApi;
	private string $userApi;
	private string $passwordApi;
	private string $urlAuth;

	private ?string $token = null;
	private string  $endPoint;
	private array   $dataResponse;

	private ResponseInterface $response;

	public function setUp(): void
	{
		self::bootKernel();
		$this->cacheStore = new FilesystemAdapter();
		$this->credentials();
	}

	protected function credentials(): void
	{
		$this->urlApi      = $_ENV['API_TEST_URL'];
		$this->userApi     = $_ENV['API_TEST_USERNAME'];
		$this->passwordApi = $_ENV['API_TEST_PASSWORD'];
		$this->ttl         = (int) $_ENV['API_TEST_TTL'];
		$this->urlAuth     = $this->urlApi . $_ENV['API_TEST_AUTH_URL'];
		$this->options     = [
			'base_uri' => $this->urlApi,
		];
	}

	public function endPoint(): string
	{
		return $this->endPoint;
	}

	public function post(string $url, mixed $data = ''): ResponseInterface
	{
		return $this->request('POST', $url, $data);
	}

	/**
	 * @throws TransportExceptionInterface
	 * @throws JsonException
	 */
	private function request(string $method, string $url, mixed $data = ''): ResponseInterface
	{
		$this->setEndPoint($url);

		if(is_array($data)){
			$dataContent = json_encode($data, JSON_THROW_ON_ERROR);
		}
		else{
			$dataContent = (empty($data)) ? json_encode([], JSON_THROW_ON_ERROR) : $data;
		}


		$this->response = $this->createApiClient()->request($method, $this->endPoint,
			[
				'headers' => [
					'Content-Type' => 'application/json'
				],
				'body'    => $dataContent
			]
		);
		return $this->prepareResponse();
	}

	private function setEndPoint(string $url): void
	{
		$this->endPoint = $this->urlApi . '/' . $url;
	}

	/**
	 * @throws TransportExceptionInterface
	 * @throws InvalidArgumentException
	 * @throws ServerExceptionInterface
	 * @throws RedirectionExceptionInterface
	 * @throws ClientExceptionInterface
	 */
	private function createApiClient(?string $token = null): Client
	{
		$token = $token ?: $this->checkCached();
		return static::createClient([], [
			'headers' => [
				'accept'        => ['application/json'],
				'authorization' => 'Bearer ' . $token
			]
		]);

	}

	/**
	 * @throws TransportExceptionInterface
	 * @throws ServerExceptionInterface
	 * @throws InvalidArgumentException
	 * @throws RedirectionExceptionInterface
	 * @throws ClientExceptionInterface
	 */
	private function checkCached()
	{
		// check if token was cached
		$cachedToken = $this->getCachedToken();
		if(null !== $cachedToken) {
			$this->token = $cachedToken;
			return $cachedToken;
		}
		return $this->getToken();

	}

	/**
	 * @throws InvalidArgumentException
	 */
	private function getCachedToken()
	{
		return $this->cacheStore->getItem($this->tokenCacheName)->get();
	}

	protected function get(string $url): ResponseInterface
	{

		return $this->request('GET', $url);

	}

	/**
	 * @throws TransportExceptionInterface
	 * @throws InvalidArgumentException
	 * @throws ServerExceptionInterface
	 * @throws RedirectionExceptionInterface
	 * @throws ClientExceptionInterface
	 */
	protected function getToken($body = []): string
	{
		self::ensureKernelShutdown();
		$response    = static::createClient($this->options)->request('POST', $this->urlAuth, [
			'headers' => [
				'Content-Type' => 'application/json'
			],
			'body'    => '{
                    "username":"' . $this->userApi . '",
                    "password":"' . $this->passwordApi . '"
                }'
		]);
		$data        = json_decode($response->getContent(), false, 512, JSON_THROW_ON_ERROR);
		$this->token = $data->token;
		$this->saveTokenToCache($this->token);
		return $this->token;

	}

	/**
	 * @throws InvalidArgumentException
	 */
	protected function saveTokenToCache(string $value): void
	{
		$cachedToken = $this->cacheStore->getItem($this->tokenCacheName);
		$cachedToken->set($value);
		$cachedToken->expiresAfter($this->ttl);
		$this->cacheStore->save($cachedToken);
	}

	private function prepareResponse(): ResponseInterface
	{
		$this->dataResponse = $this->response->toArray();
		return $this->response;
	}

	/**
	 * @throws TransportExceptionInterface
	 * @throws JsonException
	 */
	public function put(string $url, mixed $data = ''): ResponseInterface
	{
		return $this->request('PUT', $url, $data);
	}

	public function dataResponse(): array
	{
		return $this->dataResponse;
	}

	protected function response(): ResponseInterface
	{
		return $this->response;
	}

	protected function assertResponseIsOk(): void
	{
		self::assertResponseIsSuccessful();
	}

	protected function assertJsonSchema(mixed $jsonSchema): void
	{
		self::assertMatchesJsonSchema($jsonSchema);
	}

	protected function assertJsonHas(mixed $has): void
	{
		self::assertJsonContains($has);
	}

	protected function assertSchemaCollection(string $schema): void
	{

		$response = $this->dataResponse[0];
		$this->assertSchema($schema, $response);

	}

	/**
	 * @throws TransportExceptionInterface
	 * @throws ServerExceptionInterface
	 * @throws RedirectionExceptionInterface
	 * @throws ClientExceptionInterface
	 */
	protected function assertSchema(string $schema, ?array $response = null): void
	{
		$this->dataResponse = $response ?: json_decode($this->response->getContent(false), true, 512, JSON_THROW_ON_ERROR);
		$schema             = json_decode($schema, true, 512, JSON_THROW_ON_ERROR);
		$dataKeys           = array_keys($this->dataResponse);
		$schemaKeys         = array_keys($schema);
		// difference from response
		$differencesFromResponse = array_diff($schemaKeys, $dataKeys);
		foreach ($differencesFromResponse as $difference) {

			$message = sprintf('Atributo "%s", no existe en el Response', $difference);
			$this->assertContains($difference, $this->dataResponse, $message);
		}
		// differences from schema
		$differencesFromSchema = array_diff($dataKeys, $schemaKeys);
		foreach ($differencesFromSchema as $differenceSchema) {

			$messageSchema = sprintf('Atributo en Response "%s", not existe en el Schema', $differenceSchema);
			$this->assertContains($differenceSchema, $schema, $messageSchema);
		}
		$this->assertTrue(true, 'Schema y Response validos!');
	}

	/**
	 * @throws TransportExceptionInterface
	 * @throws ServerExceptionInterface
	 * @throws RedirectionExceptionInterface
	 * @throws ClientExceptionInterface
	 */
	protected function assertCountData(int $expected): void
	{

		$this->assertCount($expected, $this->dataResponse);
	}


}