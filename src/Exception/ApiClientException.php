<?php
declare(strict_types = 1);

namespace Insidesuki\ApiManager\Exception;
use RuntimeException;

class ApiClientException extends RuntimeException
{


	public function __construct($code,$message = "")
	{
		parent::__construct(sprintf('ApiClientException: The client response with code:%s, and message:%s',
			$code,$message));
	}
}