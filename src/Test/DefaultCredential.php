<?php
declare(strict_types = 1);

namespace Insidesuki\ApiManager\Test;

use Insidesuki\ApiManager\Test\Contracts\CredentialsInterface;
use Symfony;

class DefaultCredential implements CredentialsInterface
{
	private string $contentType = 'application/json';
	private string $baseUrl;
	private string $authUrl;
	private string $username;
	private string $password;
	private int    $ttl;

	public function __construct()
	{

		$dotEnv = new Symfony\Component\Dotenv\Dotenv();
		$dotEnv->load(__DIR__.'/../../config/.env.local');
		$this->baseUrl  = $_ENV['API_TEST_URL'];
		$this->authUrl  = $this->baseUrl . $_ENV['API_TEST_AUTH_URL'];
		$this->username = $_ENV['API_TEST_USERNAME'];
		$this->password = $_ENV['API_TEST_PASSWORD'];
		$this->ttl      = (int) $_ENV['API_TEST_TTL'];
	}

	public function contentType(): string
	{
		return $this->contentType;
	}

	public function baseUrl(): string
	{
		return $this->baseUrl;
	}

	public function authUrl(): string
	{
		return $this->authUrl;
	}

	public function username(): string
	{
		return $this->username;
	}

	public function password(): string
	{
		return $this->password;
	}

	public function ttl(): int
	{
		return $this->ttl;
	}


}