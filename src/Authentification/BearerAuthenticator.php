<?php
declare(strict_types = 1);

namespace Insidesuki\ApiManager\Authentification;

use Insidesuki\ApiManager\Authentification\Contracts\AuthenticatorInterface;
use InvalidArgumentException;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class BearerAuthenticator extends AbstractAuthenticator implements AuthenticatorInterface
{

	/**
	 * @throws TransportExceptionInterface
	 * @throws InvalidArgumentException
	 */
	public function auth(string $method, string $url): ResponseInterface
	{
		$urlPetition = $this->client->baseUrl() . '/' . $url;
		return $this->httpClient->request($method, $urlPetition, [
			'auth_bearer' => $this->authenticate()
		]);
	}

	/**
	 * @throws InvalidArgumentException
	 */
	public function authForBody(string $jsonBody): array
	{
		return [
			'auth_bearer' => $this->authenticate(), 'headers' => [
				'Content-Type' => $this->client->contentType()
			], 'body'     => $jsonBody
		];
	}
}