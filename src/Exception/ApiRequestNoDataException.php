<?php
declare(strict_types = 1);

namespace Insidesuki\ApiManager\Exception;

use RuntimeException;

class ApiRequestNoDataException extends RuntimeException
{

	public function __construct($message)
	{
		parent::__construct(sprintf('Url: %s, no returns any data', $message));
	}
}