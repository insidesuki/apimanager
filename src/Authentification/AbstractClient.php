<?php
declare(strict_types = 1);

namespace Insidesuki\ApiManager\Authentification;
abstract class AbstractClient
{

	protected $apiName;
	protected $baseUrl;
	protected $authUrl;
	protected $bodyAuth;
	protected $responseAuth;
	protected $expiresAt   = 3600; // seconds
	protected $contentType = 'application/json';

	public function __construct(
		string $apiName,
		string $baseUrl,
		string $authUrl,
		string $responseAuth,
		int $expiresAt = 3600,
		array $bodyAuth = []
	)
	{
		$this->apiName      = $apiName;
		$this->baseUrl      = $baseUrl;
		$this->authUrl      = $authUrl;
		$this->responseAuth = $responseAuth;
		$this->bodyAuth     = $bodyAuth;
		$this->expiresAt    = $expiresAt;
	}

	public function apiName(): string
	{
		return $this->apiName;
	}

	public function baseUrl(): string
	{
		return $this->baseUrl;
	}

	public function authUrl(): string
	{
		return $this->authUrl;
	}

	public function bodyAuth(): array
	{
		return $this->bodyAuth;
	}

	public function responseAuth(): string
	{
		return $this->responseAuth;
	}

	public function expiresAt(): int
	{
		return $this->expiresAt;
	}

	public function contentType(): string
	{
		return $this->contentType;
	}



	protected function changeContentType(string $contentType): void
	{
		$this->contentType = $contentType;
	}


}