<?php
declare(strict_types = 1);

namespace Insidesuki\ApiManager;

use Insidesuki\ApiManager\Authentification\Contracts\AuthenticatorInterface;
use Insidesuki\ApiManager\Exception\ApiClientException;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;


class ApiManager
{


	protected $authenticator;
	private   $httpClient;
	private   $response;
	private   $content;
	private   $contentType = 'application/json';
	private   $statusCode;
	public const SUCCESS_CODES = [200, 201];

	public function __construct(AuthenticatorInterface $authenticator)
	{
		$this->authenticator = $authenticator;
		$this->httpClient    = $this->authenticator->getHttpClient();

	}


	/**
	 * @throws TransportExceptionInterface
	 */
	public function find(string $url)
	{
		return $this->runRequest('GET', $url);
	}


	/**
	 * @throws TransportExceptionInterface
	 */
	protected function runRequest(string $method, string $url)
	{
		$this->response   = $this->authenticator->auth($method, $url);
		$this->statusCode = $this->response->getStatusCode();
		$this->prepareContentResponse();
		return $this->content;

	}


	private function prepareContentResponse(bool $assoc = true): void
	{


		if(in_array($this->statusCode, self::SUCCESS_CODES, true)) {
			$this->content         = json_decode($this->response->getContent(false), $assoc);
			$this->content['code'] = $this->statusCode;
		} else if($this->statusCode !== 204) {
			throw new ApiClientException(
				$this->statusCode, $this->response->getContent(false)
			);
		}


	}

	public function post(string $url, array $data)
	{

		return $this->sendData('POST', $url, $data);
	}


	public function put(string $url,array $data){

		return $this->sendData('PUT',$url,$data);
	}

	/**
	 * @param string $method
	 * @param string $url
	 * @param array $data
	 * @return mixed
	 * @throws TransportExceptionInterface
	 */
	protected function sendData(string $method, string $url, array $data)
	{

		$dataToSend       = $this->getDataByContentType($data);
		$urlPetition      = $this->authenticator->getBaseUrl() . '/' . $url;
		$body             = $this->authenticator->authForBody($dataToSend);
		$this->response   = $this->httpClient->request($method, $urlPetition, $body);
		$this->statusCode = $this->response->getStatusCode();
		$this->prepareContentResponse();
		return $this->content;

	}

	private function getDataByContentType($data)
	{

		$dataContent = '';
		switch ($this > $this->contentType) {
			case 'application/json';
				$dataContent = json_encode($data);
				break;
			case 'application/x-www-form-urlencoded';
				$dataContent = urlencode($data);
				break;
		}
		return $dataContent;
	}

	protected function changeContentType(string $contentType): void
	{
		$this->contentType = $contentType;
	}

}