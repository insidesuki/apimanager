<?php
declare(strict_types = 1);

namespace Insidesuki\ApiManager;

use Insidesuki\ApiManager\Authentification\BearerAuthenticator;
use Insidesuki\ApiManager\Authentification\Contracts\ApiBearerClientInterface;
use Insidesuki\ApiManager\Authentification\Contracts\AuthenticatorInterface;
use Insidesuki\ApiManager\Exception\ApiRequestNoDataException;
use Symfony\Contracts\HttpClient\HttpClientInterface;

abstract class AbstractApiRepository
{

	/**
	 * @var HttpClientInterface
	 */
	protected $httpClient;

	/**
	 * @var BearerAuthenticator
	 */
	protected $authClient;
	/**
	 * @var ApiManager
	 */
	private $apiManager;


	public function __construct(
		HttpClientInterface $httpClient,
		AuthenticatorInterface $authClient
	)
	{
		$this->httpClient = $httpClient;
		$this->apiManager = new ApiManager($authClient);
		$this->authClient = $authClient;

	}

	/**
	 * @param string $url
	 * @return array
	 */
	protected function findOrFail(string $url): array
	{

		$data = $this->find($url);
		if(empty($data)) {
			throw new ApiRequestNoDataException($this->authClient->getBaseUrl().'/'.$url);
		}
		return $data;

	}


	protected function find(string $url): ?array
	{

		return $this->apiManager->find($url);
	}

	protected function put(string $url, array $data): array
	{
		return $this->apiManager->put($url, $data);
	}

	protected function post(string $url, array $data)
	{
		return $this->apiManager->post($url, $data);
	}


}