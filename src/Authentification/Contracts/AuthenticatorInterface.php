<?php

namespace Insidesuki\ApiManager\Authentification\Contracts;

use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

interface AuthenticatorInterface
{


	public function auth(string $method, string $url): ResponseInterface;

	public function authForBody(string $jsonBody):  array;

	public function authenticate();

	public function refresh();

	public function getBaseUrl(): string;

	public function getHttpClient(): HttpClientInterface;

	public function getCredencial(): ApiBearerClientInterface;


}