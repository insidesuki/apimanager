<?php
declare(strict_types = 1);

namespace Insidesuki\ApiManager\Exception;

use RuntimeException;

class ApiException extends RuntimeException
{

	public function __construct($message = "")
	{
		parent::__construct(sprintf('ApiManagerException %s',$message));
	}

}