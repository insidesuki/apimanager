**API-MANAGER**
==============================

Wrapper for Api-Rest petition.

Complete documentation in : https://gitlab.com/insidesuki/apimanager

> Supports BearerAuth.

**1-Use the Manager**
===
---

-  Create a Client
```
    use Insidesuki\ApiManager\Authentification\DefaultBearerClient;
  
    
    // config the client
    // credentials taken from .env file
    $apiClient = new DefaultBearerClient(
		$_ENV['API_TEST1_NAME'],
		$_ENV['API_TEST1_BASE_URL'],
		$_ENV['API_TEST1_AUTH_URL'],
		$_ENV['API_TEST1_TOKEN_NAME']
	);
	
```

- Create Authenticator
> example in BearerAuthenticator and Symfony httpclient component.
```
    use Insidesuki\ApiManager\Authentification\BearerAuthenticator;
    
    $apiAuth = new BearerAuthenticator(
		new NativeHttpClient(),
		$apiClient
	);
	
```

- Create  manager

```
    use Insidesuki\ApiManager\Authentification\BearerAuthenticator;
    
    $apiManager = new ApiManager($apiAuth);
	
```



**2-Usage as a repository**
==============================
---
- Create a new repo

```
    use Insidesuki\ApiManager\DefaultBearerRepository;
    use Symfony\Component\HttpClient\NativeHttpClient;
    
	$apiRepo = new DefaultBearerApiRepository(
		new NativeHttpClient(),
		$apiClient
	);


```

- find:
> $result = $apiRepo->find('api/endpoint/params');

- findOrFail, throw exception
> $result = $apiRepo->findOrFail('api/endpoint/params');

- put

````

	$putData = [
		'data' => [
			[
				'field' => 'value'
			]
		]
	];
	
	$result = $apiRepo->put('api/endPoint/{id}',$data);

````

**3-Test your own endpoint**
==============================
---

**3.1-Preparing the envorinment**
--



- Set in your .env.local test:

````

###> api self tests###
API_TEST_URL='https://127.0.0.1:8000/api'
API_TEST_USERNAME='USER'
API_TEST_PASSWORD='PASS'
API_TEST_AUTH_URL='/auth'
API_TEST_TTL=60
###< seltest-api-credentials###

````

- Make sure that in your .env.test file, the following keys exist:

````
###> lexik/jwt-authentication-bundle ###
JWT_SECRET_KEY=%kernel.project_dir%/config/jwt/private.pem
JWT_PUBLIC_KEY=%kernel.project_dir%/config/jwt/public.pem
JWT_PASSPHRASE=A_PASS_PHRASE
###< lexik/jwt-authentication-bundle ###

````

- Make sure that in the xml of phpunit.xml.dist there are no lexik/jwt-authentication-bundle keys
- Create a new test for your ActionController
- Extends the new test from Insidesuki\ApiManager\Test\MyApiPlatformTest


**3.2-Testing GET request**
---


````
public function testOk()
	{

		$this->get('tarea/18567');
		
		// test if response code is 200 or 201 or 204
		$this->assertResponseIsOk();
		
		// test if api response data is equals to a json schema (json string)
		$this->assertSchema($this->jsonSchema);
		
		// test count
		$this->assertCountData(4);

	}

````

**3.3-Testing POST/PUT request**
---

POST request, is same as a GET request, but it needs a jsonData or array of data,as second argument.

    {

		$this->POST('tarea','{"tarea":"tareaTest"}');
		
		// test if response code is 200 or 201 or 204
		$this->assertResponseIsOk();
		
		

	}


```



```
