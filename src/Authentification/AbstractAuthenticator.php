<?php
declare(strict_types = 1);

namespace Insidesuki\ApiManager\Authentification;

use Insidesuki\ApiManager\Authentification\Contracts\ApiBearerClientInterface;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

abstract class AbstractAuthenticator
{
	protected const METTHOD_AUTH = 'POST';

	protected $cacheStore;
	/**
	 * @var bool
	 */
	protected $storeInCache;
	/**
	 * @var HttpClientInterface
	 */
	protected $httpClient;
	/**
	 * @var mixed
	 */
	protected $tokenName;

	protected $content;
	/**
	 * @var ApiBearerClientInterface
	 */
	protected $client;


	public function __construct(
		HttpClientInterface $httpClient,
		ApiBearerClientInterface $credential,
		bool $storeInCache = true)
	{

		$this->httpClient   = $httpClient;
		$this->client       = $credential;
		$this->storeInCache = $storeInCache;
		$this->cacheStore   = new FilesystemAdapter();
		$this->tokenName    = 'api_name.' . $this->client->apiName();
	}

	/**
	 * @throws InvalidArgumentException
	 */
	public function refresh(bool $auth = true)
	{
		$this->cacheStore->delete($this->tokenName);
		return ($auth) ? $this->authenticate() : true;
	}

	/**
	 * @throws InvalidArgumentException
	 */
	public function authenticate()
	{

		if($this->storeInCache) {
			return $this->checkCached();
		}
		{
			return $this->doAuth();
		}

	}

	/**
	 * Remove token from cache
	 * @return void
	 * @throws InvalidArgumentException
	 */
	public function invalidateToken(): void
	{
		$this->refresh(false);
	}

	/**
	 * @throws InvalidArgumentException
	 */
	private function checkCached()
	{

		// check if token was cached
		$cachedToken = $this->getCachedToken();
		if(null !== $cachedToken) {
			return $cachedToken;
		}
		return $this->doAuth();

	}

	/**
	 * Get token value stored in cache system
	 * @throws InvalidArgumentException
	 */
	private function getCachedToken()
	{
		return $this->cacheStore->getItem($this->tokenName)->get();

	}

	/**
	 * @throws TransportExceptionInterface
	 * @throws InvalidArgumentException
	 * @throws ServerExceptionInterface
	 * @throws RedirectionExceptionInterface
	 * @throws DecodingExceptionInterface
	 * @throws ClientExceptionInterface
	 */
	protected function doAuth(): string
	{

		$response      = $this->httpClient->request(
			self::METTHOD_AUTH, $this->client->authUrl(), $this->client->bodyAuth()
		);
		$code          = $response->getStatusCode();
		$this->content = $response->toArray();
		$responseAuth  = $this->content[$this->client->responseAuth()];
		$this->saveTokenToCache($responseAuth);
		return $responseAuth;


	}


	/**
	 * @throws InvalidArgumentException
	 */
	protected function saveTokenToCache(string $value): void
	{
		if($this->storeInCache) {
			$cachedToken = $this->cacheStore->getItem($this->tokenName);
			$cachedToken->set($value);
			$cachedToken->expiresAfter($this->client->expiresAt());
			$this->cacheStore->save($cachedToken);
		}

	}

	public function getCredencial(): ApiBearerClientInterface
	{
		return $this->client;
	}

	public function getBaseUrl(): string
	{
		return $this->client->baseUrl();
	}

	public function getHttpClient(): HttpClientInterface
	{
		return $this->httpClient;
	}

	protected function getContent(): array
	{

		return $this->content;
	}


}