<?php

namespace Insidesuki\ApiManager\Authentification\Contracts;

interface ApiBearerClientInterface
{

	public function contentType(): string;

	public function apiName():string;


	public function baseUrl():string;


	public function authUrl():string;


	public function bodyAuth(): array;


	public function responseAuth(): string;


	public function expiresAt(): int;

}